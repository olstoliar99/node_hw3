const LOAD_STATES = [
  "En route to Pick Up",
  "Arrived to Pick Up",
  "En route to delivery",
  "Arrived to delivery"
];

const LOAD_STATUSES = {
  NEW: "NEW",
  POSTED: "POSTED",
  ASSIGNED: "ASSIGNED",
  SHIPPED: "SHIPPED"
};

const ROLES = ["DRIVER", "SHIPPER"];

const TRUCK_TYPES = ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"];

const TRUCK_STATUSES = {
  OL: "OL",
  IS: "IS"
};

module.exports = {
  LOAD_STATES,
  LOAD_STATUSES,
  ROLES,
  TRUCK_TYPES,
  TRUCK_STATUSES
};
