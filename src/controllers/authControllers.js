/* eslint-disable no-underscore-dangle */
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const uuid = require("uuid");

require("dotenv").config();

const { User, userJoiSchema } = require("../models/userModel");
const { sentPasswordToEmail } = require("../helpers/sentPasswordToEmail");

const registerUser = async (req, res) => {
  const { email, password, role } = req.body;

  await userJoiSchema.validateAsync({ email, password, role });

  const user = new User({
    email,
    password: bcrypt.hashSync(
      password,
      bcrypt.genSaltSync(+process.env.ENCODE_ROUNDS)
    ),
    role
  });

  return user.save().then(() => res.status(200).send({ message: "Success" }));
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  await userJoiSchema.validateAsync({ email, password });

  const currentUser = await User.findOne({ email });

  if (!currentUser) {
    return res.status(400).json({ message: "User profile is not found" });
  }

  // passwords is matches
  if (bcrypt.compareSync(password, currentUser.password)) {
    const token = jwt.sign(
      {
        _id: currentUser._id,
        email
      },
      process.env.SECRET_KEY
      // { expiresIn: '15m' },
    );

    return res.status(200).json({
      jwt_token: token
    });
  }

  return res.status(400).json({
    message: "Passwords do not match"
  });
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;

  await userJoiSchema.validateAsync({ email });

  const currentUser = await User.findOne({ email });

  const newPassword = uuid.v4();
  const encryptedPassword = bcrypt.hashSync(
    newPassword,
    bcrypt.genSaltSync(+process.env.ENCODE_ROUNDS)
  );
  currentUser.password = encryptedPassword;

  console.log(newPassword);

  await currentUser.save();

  // sent new password to the given email address
  sentPasswordToEmail(email, newPassword);

  return res.status(200).json({
    message: "New password sent to your email address"
  });
};

module.exports = {
  registerUser,
  loginUser,
  forgotPassword
};
