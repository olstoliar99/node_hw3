const { Truck, truckJoiSchema } = require("../models/truckModel");

const getUserTrucks = async (req, res) => {
  const { _id: driverId } = req.currentUserProfile;

  const userTrucks = await Truck.find(
    { created_by: driverId },
    "_id created_by assigned_to type status created_date"
  );

  if (userTrucks.length === 0) {
    return res.status(400).json({ message: "Trucks are not yet created" });
  }

  console.log(userTrucks);

  return res.status(200).json({
    trucks: userTrucks
  });
};

const addUserTrucks = async (req, res) => {
  const { _id: driverId } = req.currentUserProfile;
  const { type } = req.body;

  const truck = new Truck({
    created_by: driverId,
    type,
    status: "IS"
  });

  await truck.save().then((truckSaved) => {
    console.log(truckSaved);
  });

  return res.status(200).json({
    message: "Truck created successfully"
  });
};

const getUserTruckById = async (req, res) => {
  const { id } = req.params;
  const { _id: userId, email } = req.currentUserProfile;

  const userTruck = await Truck.find(
    { _id: id, created_by: userId },
    "_id created_by assigned_to type status created_date"
  );

  if (!userTruck) {
    return res
      .status(400)
      .json({ message: `Trucks for ${email} is not found` });
  }

  console.log(userTruck);

  return res.status(200).json({
    truck: userTruck
  });
};

const updateUserTruckById = async (req, res) => {
  const { id } = req.params;
  const { type } = req.body;

  const { _id: userId } = req.currentUserProfile;

  await truckJoiSchema.validateAsync({ type });

  const truckToUpdate = await Truck.findOne({
    _id: id,
    created_by: userId,
    assigned_to: { $ne: userId }
  });

  if (!truckToUpdate) {
    return res.status(400).json({
      message: `Driver ${userId} can't update assigned to himself trucks`
    });
  }

  truckToUpdate.type = type;

  await truckToUpdate.save();

  console.log(truckToUpdate);

  return res.status(200).json({
    message: "Truck details changed successfully"
  });
};

const deleteUserTruckById = async (req, res) => {
  const { id } = req.params;
  const { _id: userId } = req.currentUserProfile;

  const truckToDelete = await Truck.findOne({
    _id: id,
    created_by: userId,
    assigned_to: { $ne: userId }
  });

  if (!truckToDelete) {
    return res
      .status(400)
      .json({
        message: `Driver ${userId} can't delete assigned to himself trucks`
      });
  }

  await truckToDelete.delete();

  return res.status(200).json({
    message: "Truck deleted successfully"
  });
};

const assignUserTruckById = async (req, res) => {
  const { id } = req.params;
  const { _id: userId, email } = req.currentUserProfile;

  const truckToAssign = await Truck.findById(id);

  if (truckToAssign.assigned_to.equals(userId)) {
    return res
      .status(400)
      .json({ message: `The truck has been assigned by ${email} before` });
  }

  const assignedTruckForUser = await Truck.findOne({ assigned_to: userId });

  if (assignedTruckForUser) {
    return res.status(400).json({
      message: `The driver ${email} can't assign more then one truck`
    });
  }

  truckToAssign.assigned_to = userId;

  await truckToAssign.save();

  console.log(truckToAssign);

  return res.status(200).json({
    message: "Truck assigned successfully"
  });
};

module.exports = {
  getUserTrucks,
  addUserTrucks,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById
};
