const express = require("express");

const {
  getUserTrucks,
  addUserTrucks,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById
} = require("../controllers/truckController");

const {
  truckOnLoadGuardMiddleware
} = require("../middleware/truckOnLoadGuardMiddleware");
const { asyncWrapper } = require("../helpers/asyncWrapper");

const router = express.Router();

router
  .get("", asyncWrapper(getUserTrucks))
  .post("", asyncWrapper(addUserTrucks))
  .get("/:id", asyncWrapper(getUserTruckById))
  .put("/:id", truckOnLoadGuardMiddleware, asyncWrapper(updateUserTruckById))
  .delete("/:id", truckOnLoadGuardMiddleware, asyncWrapper(deleteUserTruckById))
  .post(
    "/:id/assign",
    truckOnLoadGuardMiddleware,
    asyncWrapper(assignUserTruckById)
  );

module.exports = {
  truckRouter: router
};
