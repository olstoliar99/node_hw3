const express = require("express");

const {
  createLoad,
  nextActiveState,
  postUserLoadById,
  getActiveLoad,
  getUserLoads,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  getLoadShippingInfoById
} = require("../controllers/loadController");

const {
  shipperGuardMiddleware
} = require("../middleware/shipperGuardMiddleware");
const {
  driverGuardMiddleware
} = require("../middleware/driverGuardMiddleware");
const { asyncWrapper } = require("../helpers/asyncWrapper");

const router = express.Router();

router
  .get("", asyncWrapper(getUserLoads))
  .post("", shipperGuardMiddleware, asyncWrapper(createLoad))
  .patch("/active/state", driverGuardMiddleware, asyncWrapper(nextActiveState))
  .post("/:id/post", shipperGuardMiddleware, asyncWrapper(postUserLoadById))
  .get("/active", driverGuardMiddleware, asyncWrapper(getActiveLoad))
  .get("/:id", asyncWrapper(getUserLoadById))
  .put("/:id", shipperGuardMiddleware, asyncWrapper(updateUserLoadById))
  .delete("/:id", shipperGuardMiddleware, asyncWrapper(deleteUserLoadById))
  .get(
    "/:id/shipping_info",
    shipperGuardMiddleware,
    asyncWrapper(getLoadShippingInfoById)
  );

module.exports = {
  loadsRouter: router
};
