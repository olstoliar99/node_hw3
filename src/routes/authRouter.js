const express = require("express");

const {
  registerUser,
  loginUser,
  forgotPassword
} = require("../controllers/authControllers");

const { asyncWrapper } = require("../helpers/asyncWrapper");

const router = express.Router();

router.post("/register", asyncWrapper(registerUser));

router.post("/login", asyncWrapper(loginUser));

router.post("/forgot_password", asyncWrapper(forgotPassword));

module.exports = {
  authRouter: router
};
