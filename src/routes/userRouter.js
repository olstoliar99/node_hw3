const express = require("express");

const {
  getUserProfile,
  deleteUserProfile,
  updateUserPassword
} = require("../controllers/userControllers");

const { asyncWrapper } = require("../helpers/asyncWrapper");

const router = express.Router();

router
  .get("", asyncWrapper(getUserProfile))
  .delete("", asyncWrapper(deleteUserProfile))
  .patch("", asyncWrapper(updateUserPassword));

module.exports = {
  userRouter: router
};
