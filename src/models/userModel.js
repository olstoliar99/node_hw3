const { Schema, model } = require("mongoose");

const Joi = require("joi");

const { ROLES } = require("../constants");

const userJoiSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]/)
    .optional(),
  role: Joi.string()
    .regex(/DRIVER|SHIPPER/)
    .optional()
});

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true,
    enum: ROLES
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
    required: true
  }
});

const User = model("users", userSchema);

module.exports = {
  User,
  userJoiSchema
};
