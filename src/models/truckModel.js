const { Types, Schema, model } = require("mongoose");

const Joi = require("joi");

const { TRUCK_TYPES, TRUCK_STATUSES } = require("../constants");

const truckJoiSchema = Joi.object().keys({
  type: Joi.string()
    .custom((type, helper) =>
      TRUCK_TYPES.includes(type)
        ? type
        : helper.message(
            `Type must be defined as some of these values: ${TRUCK_TYPES.join(
              ", "
            )}`
          )
    )
    .required()
});

const truckSchema = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    required: true
  },
  assigned_to: {
    type: Types.ObjectId,
    default: Types.ObjectId,
    required: true,
    turnOn: true
  },
  type: {
    type: String,
    required: true,
    enum: TRUCK_TYPES
  },
  status: {
    type: String,
    required: true,
    enum: Object.values(TRUCK_STATUSES)
  },
  created_date: {
    type: String,
    default: new Date().toISOString(),
    required: true
  }
});

const Truck = model("trucks", truckSchema);

module.exports = {
  Truck,
  truckJoiSchema
};
