const { LOAD_STATUSES } = require("../constants");

const switchLoadStatus = (status) => {
  const nextStatusIndex =
    (1 + Object.keys(LOAD_STATUSES).indexOf(status)) %
    Object.keys(LOAD_STATUSES).length;
  const nextStatus = Object.keys(LOAD_STATUSES)[nextStatusIndex];

  console.log(nextStatus);

  return nextStatus;
};

module.exports = {
  switchLoadStatus
};
