const sendMail = require("@sendgrid/mail");

require("dotenv").config();

// sendMail.setApiKey(process.env.SENDGRID_API_KEY);

const sentPasswordToEmail = async (email, password) => {
  try {
    await sendMail.send({
      to: {
        email
      },
      from: {
        email: process.env.MY_SECRET_EMAIL
      },
      templateId: process.env.SENDGRID_TEMPLATE_ID,
      dynamicTemplateData: {
        email,
        password
      }
    });
  } catch (e) {
    console.log(e.message);
  }
};

module.exports = {
  sentPasswordToEmail
};
