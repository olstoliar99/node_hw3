const { LOAD_STATES } = require("../constants");

const switchLoadState = (state) => {
  const nextStateIndex = (1 + LOAD_STATES.indexOf(state)) % LOAD_STATES.length;
  const nextState = LOAD_STATES[nextStateIndex];

  console.log(nextState);

  return nextState;
};

module.exports = {
  switchLoadState
};
