const shipperGuardMiddleware = async (req, res, next) => {
  const { role } = req.currentUserProfile;

  if (String(role).toLowerCase() === "driver") {
    return res.status(400).json({ message: "Available only for shipper role" });
  }

  return next();
};

module.exports = {
  shipperGuardMiddleware
};
