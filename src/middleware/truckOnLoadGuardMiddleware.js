const { Truck } = require("../models/truckModel");

const truckOnLoadGuardMiddleware = async (req, res, next) => {
  const { id: truckId } = req.params;

  const truck = await Truck.findById(truckId);

  if (truck.status === "OL") {
    return res.status(400).json({
      message: "Driver cannot change truck info if it's on load"
    });
  }

  return next();
};

module.exports = {
  truckOnLoadGuardMiddleware
};
