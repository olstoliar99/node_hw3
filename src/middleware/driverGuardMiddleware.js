const driverGuardMiddleware = async (req, res, next) => {
  const { role } = req.currentUserProfile;

  if (role === "SHIPPER") {
    return res.status(400).json({ message: "Available only for driver role" });
  }

  return next();
};

module.exports = {
  driverGuardMiddleware
};
